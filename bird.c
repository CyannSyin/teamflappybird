#include <curses.h>
#include <math.h>
#include "bird.h"

#define BIRD_REPR "є( ･Θ･)э"

//获取鸟的字符串
char *get_bird_repr(void) {
    return BIRD_REPR;
}

//打印
void print_bird(struct bird *b) {
    mvprintw(b->y, b->x, get_bird_repr());
    refresh();
}

//移动
void move_bird(struct bird *b) {
    // 让鸟通过加速度acc向y轴方向移动
    b->y += (int) round(b->acc);
}