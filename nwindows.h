#ifndef FLAPPY_BIRD_NWINDOWS_H
#define FLAPPY_BIRD_NWINDOWS_H

struct WIN_pos_size {
    int startx, starty;
    int width, height;
};

#endif //FLAPPY_BIRD_NWINDOWS_H
