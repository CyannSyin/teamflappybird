#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include "curses.h" // 引入curses头文件
#include "flag.h"
#include "game.h"

#define FILE_FLAPPY_WELCOME_TITLE  "res/title.txt"
#define FILE_FLAPPY_WELCOME_INSTRUCTIONS "res/instructions.txt"
#define FILE_FLAPPY_WELCOME_CREDITS "res/credits.txt"

WINDOW *wscore = NULL;

void config_ncurses(void) {
    initscr(); // 初始化，进入NCURSES模式
    noecho(); // 不回显字符
    cbreak(); // 禁用行缓冲
    curs_set(FALSE); //不显示光标
}

int count_file_chars(FILE* fp) {

    unsigned int chars = 0;
    int c;
//    int fd = open(fp,O_RDONLY);
//    //使用lseek()获取字符数
//    int flen;
//    flen = lseek(fd,0,SEEK_END);
//    chars = flen/8;


    for (c = getc(fp); c != EOF; c = getc(fp)) {
        chars++;
    }

    // 将当前文件指针置于0
    if (fseek(fp, 0L, SEEK_SET) != 0) {
        fprintf(stderr, "Repositioning error.");
        endwin();
        exit(1);
    }


    return chars;
}

void print_file(char *filename, unsigned int x, unsigned int y) {
    FILE *fp;
    if ((fp = fopen(filename, "r")) != NULL) {
        char content[count_file_chars(fp)];
        size_t i = 0;
        int c;
        for (c = getc(fp); c != EOF; c = getc(fp)) {
            content[i++] = c;
        }
        mvprintw(y, x, content);
        fclose(fp);
    } else {
        fprintf(stderr, "cant open file: %s", filename);
        endwin();
        exit(1);
    }
}

void print_title_screen() {
    print_file(FILE_FLAPPY_WELCOME_TITLE, 0, 0);
    print_file(FILE_FLAPPY_WELCOME_INSTRUCTIONS, 0, 20);
    print_file(FILE_FLAPPY_WELCOME_CREDITS, 0, 30);
}

//是否进入游戏
unsigned char do_u_want_to_play() {
    while (1) {
        char ch = getch();
        if (ch == YES)
            return 1;
        else if (ch == ESC)
            return 0;
        usleep(100000);
    }
}

int main() {
    config_ncurses();
    print_title_screen();
    refresh();

    if (do_u_want_to_play()) {
        clear();
        //start_game();
        getch();
    }

    printw("New line");

    endwin();
    return 0;
}
