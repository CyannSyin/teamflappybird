// 鸟的定义
struct bird {
    int x; // x坐标
    int y; // y坐标
    float acc; // 加速度
};

// 获取鸟的字符串
char *get_bird_repr(void);

// 打印鸟
void print_bird(struct bird *b);

// 移动鸟
void move_bird(struct bird *b);