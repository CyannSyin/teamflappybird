#ifndef FLAPPY_BIRD_PIPE_H
#define FLAPPY_BIRD_PIPE_H

struct WIN_pos_size;

// 管道
struct pipe {
    char width;
    int y;
};

// 障碍对
struct pipe_pair {
    int x;
    struct pipe obs_up;
    struct pipe obs_down;
};

// 管道设置
struct pipe_setting {
    unsigned int width;
    int spacing;
    int min_vert_gap;
    int max_vert_diff_neighbors;
};

// 管道初始化
void init_pipes(struct WIN_pos_size win, struct pipe_pair *pairs, unsigned int, struct pipe_setting setting);

// 打印
void print_pipes(struct WIN_pos_size win, struct pipe_pair *pairs, unsigned int, struct pipe_setting setting);

// 移动管道
void advance_pipes(struct WIN_pos_size win, struct pipe_pair *pairs, unsigned int, struct pipe_setting setting);

#endif //FLAPPY_BIRD_pipeS_H
